###############################################################################
#
# 	The CentOS Rolling Notes Makefile
# 	Copyright (C) 2019  Alain Reguera Delgado
#
# 	This program is free software: you can redistribute it and/or modify it
# 	under the terms of the GNU General Public License as published by the Free
# 	Software Foundation, either version 3 of the License, or (at your option)
# 	any later version.
#
# 	This program is distributed in the hope that it will be useful, but WITHOUT
# 	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# 	FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# 	more details.
#
# 	You should have received a copy of the GNU General Public License along
# 	with this program.  If not, see <https://www.gnu.org/licenses/>.
#
###############################################################################

#==============================================================================
# Configuration
#==============================================================================
lang = en_US
srcdir = SVG
prefix = PNG
datarootdir = $(prefix)/$(lang)
localedir = PO

#==============================================================================
# Requirements
#==============================================================================
all: \
	$(localedir)/$(lang).pot \
	$(localedir)/$(lang).po \
	$(datarootdir)/01-centos-welcome.png \
	$(datarootdir)/02-centos-sig-core.png \
	$(datarootdir)/03-centos-sig-altarch.png \
	$(datarootdir)/04-centos-sig-artwork.png \
	$(datarootdir)/05-centos-sig-atomic.png \
	$(datarootdir)/06-centos-sig-cloud.png \
	$(datarootdir)/07-centos-sig-cloudinstance.png \
	$(datarootdir)/08-centos-sig-cm.png \
	$(datarootdir)/09-centos-sig-nfv.png \
	$(datarootdir)/10-centos-sig-optools.png \
	$(datarootdir)/11-centos-sig-paas.png \
	$(datarootdir)/12-centos-sig-promo.png \
	$(datarootdir)/13-centos-sig-publicci.png \
	$(datarootdir)/14-centos-sig-sc.png \
	$(datarootdir)/15-centos-sig-storage.png \
	$(datarootdir)/16-centos-sig-virt.png

#======================================================================
# Translations
#======================================================================
$(localedir)/$(lang).pot: $(srcdir)/*.svg
	xml2po -o /tmp/$(lang).pot $(srcdir)/*.svg
	if [[ ! -f $(localedir)/$(lang).pot ]];then \
		msginit -i /tmp/$(lang).pot -o $(localedir)/$(lang).pot -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $(localedir)/$(lang).pot /tmp/$(lang).pot; \
	fi
$(localedir)/$(lang).po: $(localedir)/$(lang).pot
	if [[ ! -f $(localedir)/$(lang).po ]];then \
		msginit -i $(localedir)/$(lang).pot -o $(localedir)/$(lang).po -l $(lang) --no-translator --width=80; \
	else \
		msgmerge -U $(localedir)/$(lang).po $(localedir)/$(lang).pot; \
	fi

#======================================================================
# Images
#======================================================================
$(datarootdir):
	mkdir -p $(datarootdir)
$(datarootdir)/01-centos-welcome.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/01-centos-welcome.svg 
	xmllint -noout $(srcdir)/01-centos-welcome.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/01-centos-welcome.svg $(srcdir)/01-centos-welcome.svg
	inkscape --export-png=$(datarootdir)/01-centos-welcome.png /tmp/01-centos-welcome.svg
	rm /tmp/01-centos-welcome.svg
$(datarootdir)/02-centos-sig-core.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/02-centos-sig-core.svg
	xmllint -noout $(srcdir)/02-centos-sig-core.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/02-centos-sig-core.svg $(srcdir)/02-centos-sig-core.svg
	inkscape --export-png=$(datarootdir)/02-centos-sig-core.png /tmp/02-centos-sig-core.svg
	rm /tmp/02-centos-sig-core.svg
$(datarootdir)/03-centos-sig-altarch.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/03-centos-sig-altarch.svg
	xmllint -noout $(srcdir)/03-centos-sig-altarch.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/03-centos-sig-altarch.svg $(srcdir)/03-centos-sig-altarch.svg
	inkscape --export-png=$(datarootdir)/03-centos-sig-altarch.png /tmp/03-centos-sig-altarch.svg
	rm /tmp/03-centos-sig-altarch.svg
$(datarootdir)/04-centos-sig-artwork.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/04-centos-sig-artwork.svg
	xmllint -noout $(srcdir)/04-centos-sig-artwork.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/04-centos-sig-artwork.svg $(srcdir)/04-centos-sig-artwork.svg
	inkscape --export-png=$(datarootdir)/04-centos-sig-artwork.png /tmp/04-centos-sig-artwork.svg
	rm /tmp/04-centos-sig-artwork.svg
$(datarootdir)/05-centos-sig-atomic.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/05-centos-sig-atomic.svg
	xmllint -noout $(srcdir)/05-centos-sig-atomic.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/05-centos-sig-atomic.svg $(srcdir)/05-centos-sig-atomic.svg
	inkscape --export-png=$(datarootdir)/05-centos-sig-atomic.png /tmp/05-centos-sig-atomic.svg
	rm /tmp/05-centos-sig-atomic.svg
$(datarootdir)/06-centos-sig-cloud.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/06-centos-sig-cloud.svg
	xmllint -noout $(srcdir)/06-centos-sig-cloud.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/06-centos-sig-cloud.svg $(srcdir)/06-centos-sig-cloud.svg
	inkscape --export-png=$(datarootdir)/06-centos-sig-cloud.png /tmp/06-centos-sig-cloud.svg
	rm /tmp/06-centos-sig-cloud.svg
$(datarootdir)/07-centos-sig-cloudinstance.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/07-centos-sig-cloudinstance.svg
	xmllint -noout $(srcdir)/07-centos-sig-cloudinstance.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/07-centos-sig-cloudinstance.svg $(srcdir)/07-centos-sig-cloudinstance.svg
	inkscape --export-png=$(datarootdir)/07-centos-sig-cloudinstance.png /tmp/07-centos-sig-cloudinstance.svg
	rm /tmp/07-centos-sig-cloudinstance.svg
$(datarootdir)/08-centos-sig-cm.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/08-centos-sig-cm.svg
	xmllint -noout $(srcdir)/08-centos-sig-cm.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/08-centos-sig-cm.svg $(srcdir)/08-centos-sig-cm.svg
	inkscape --export-png=$(datarootdir)/08-centos-sig-cm.png /tmp/08-centos-sig-cm.svg
	rm /tmp/08-centos-sig-cm.svg
$(datarootdir)/09-centos-sig-nfv.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/09-centos-sig-nfv.svg
	xmllint -noout $(srcdir)/09-centos-sig-nfv.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/09-centos-sig-nfv.svg $(srcdir)/09-centos-sig-nfv.svg
	inkscape --export-png=$(datarootdir)/09-centos-sig-nfv.png /tmp/09-centos-sig-nfv.svg
	rm /tmp/09-centos-sig-nfv.svg
$(datarootdir)/10-centos-sig-optools.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/10-centos-sig-optools.svg
	xmllint -noout $(srcdir)/10-centos-sig-optools.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/10-centos-sig-optools.svg $(srcdir)/10-centos-sig-optools.svg
	inkscape --export-png=$(datarootdir)/10-centos-sig-optools.png /tmp/10-centos-sig-optools.svg
	rm /tmp/10-centos-sig-optools.svg
$(datarootdir)/11-centos-sig-paas.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/11-centos-sig-paas.svg
	xmllint -noout $(srcdir)/11-centos-sig-paas.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/11-centos-sig-paas.svg $(srcdir)/11-centos-sig-paas.svg
	inkscape --export-png=$(datarootdir)/11-centos-sig-paas.png /tmp/11-centos-sig-paas.svg
	rm /tmp/11-centos-sig-paas.svg
$(datarootdir)/12-centos-sig-promo.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/12-centos-sig-promo.svg
	xmllint -noout $(srcdir)/12-centos-sig-promo.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/12-centos-sig-promo.svg $(srcdir)/12-centos-sig-promo.svg
	inkscape --export-png=$(datarootdir)/12-centos-sig-promo.png /tmp/12-centos-sig-promo.svg
	rm /tmp/12-centos-sig-promo.svg
$(datarootdir)/13-centos-sig-publicci.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/13-centos-sig-publicci.svg
	xmllint -noout $(srcdir)/13-centos-sig-publicci.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/13-centos-sig-publicci.svg $(srcdir)/13-centos-sig-publicci.svg
	inkscape --export-png=$(datarootdir)/13-centos-sig-publicci.png /tmp/13-centos-sig-publicci.svg
	rm /tmp/13-centos-sig-publicci.svg
$(datarootdir)/14-centos-sig-sc.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/14-centos-sig-sc.svg
	xmllint -noout $(srcdir)/14-centos-sig-sc.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/14-centos-sig-sc.svg $(srcdir)/14-centos-sig-sc.svg
	inkscape --export-png=$(datarootdir)/14-centos-sig-sc.png /tmp/14-centos-sig-sc.svg
	rm /tmp/14-centos-sig-sc.svg
$(datarootdir)/15-centos-sig-storage.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/15-centos-sig-storage.svg
	xmllint -noout $(srcdir)/15-centos-sig-storage.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/15-centos-sig-storage.svg $(srcdir)/15-centos-sig-storage.svg
	inkscape --export-png=$(datarootdir)/15-centos-sig-storage.png /tmp/15-centos-sig-storage.svg
	rm /tmp/15-centos-sig-storage.svg
$(datarootdir)/16-centos-sig-virt.png: $(datarootdir) $(localedir)/$(lang).po $(srcdir)/16-centos-sig-virt.svg
	xmllint -noout $(srcdir)/16-centos-sig-virt.svg
	xml2po -p $(localedir)/$(lang).po -o /tmp/16-centos-sig-virt.svg $(srcdir)/16-centos-sig-virt.svg
	inkscape --export-png=$(datarootdir)/16-centos-sig-virt.png /tmp/16-centos-sig-virt.svg
	rm /tmp/16-centos-sig-virt.svg
